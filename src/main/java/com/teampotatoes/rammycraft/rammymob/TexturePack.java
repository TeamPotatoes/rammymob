package com.teampotatoes.rammycraft.rammymob;

public enum TexturePack {
    // Lead
    // Capture Net(s)
    CAPTURENET_EMPTY(1000),
    CAPTURENET_PANDA(1001),
    CAPTURENET_BEE(1002);

    private final int customModelData;

    TexturePack(final int customModelData) {
        this.customModelData = customModelData;
    }

    public int getId() {
        return this.customModelData;
    }
}
