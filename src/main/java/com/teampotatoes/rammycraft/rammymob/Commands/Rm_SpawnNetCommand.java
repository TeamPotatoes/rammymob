package com.teampotatoes.rammycraft.rammymob.Commands;

import com.teampotatoes.rammycraft.rammymob.RammyMob;
import com.teampotatoes.rammycraft.rammymob.TexturePack;
import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class Rm_SpawnNetCommand implements CommandExecutor
{
    private RammyMob instance;

    public Rm_SpawnNetCommand(RammyMob instance)
    {
        this.instance = instance;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        Player player = null;
        if (args.length == 1)
        {
            // Sender is console
            if (sender instanceof ConsoleCommandSender)
            {
                sender.sendMessage("Player need to be defined");
                return true;
            }
        }
        if (args.length > 1)
        {
            player = Bukkit.getPlayer(args[1]);
            if (player == null)
            {
                sender.sendMessage("Player is not online");
                return true;
            }
        } else
        {
            // Fallback to sender
            player = (Player) sender;
        }
        ItemStack captureNet = new ItemStack(Material.LEAD);
        ItemMeta itemMeta = captureNet.getItemMeta();
        ArrayList<String> lores = new ArrayList<>();
        lores.add(ChatColor.GOLD + "Right click on an entity to capture it. (Only works in ExploreWorld)");
        lores.add(ChatColor.GOLD + "Supported Entities:");
        lores.add(ChatColor.GREEN + "- Panda");
        lores.add(ChatColor.GREEN + "- Bee");
        lores.add(ChatColor.RED + "This is experimental! Things may break, use with caution!");
        itemMeta.setLore(lores);
        itemMeta.setCustomModelData(TexturePack.CAPTURENET_EMPTY.getId()); // Set data for texture pack
        itemMeta.setDisplayName(ChatColor.RESET + "" + ChatColor.AQUA + "Capture Net"); // Set capture net name
        captureNet.setItemMeta(itemMeta);

        NBTItem nbtItem = new NBTItem(captureNet);
        nbtItem.setInteger("isCaptureNet", 1);
        nbtItem.setString("UUID", UUID.randomUUID().toString());

        player.getInventory().addItem(nbtItem.getItem());
        return true;
    }
}
