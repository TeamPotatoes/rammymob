package com.teampotatoes.rammycraft.rammymob.Commands;

import com.teampotatoes.rammycraft.rammymob.RammyMob;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RmCommand implements CommandExecutor
{
    private final RammyMob instance;

    public RmCommand(RammyMob instance)
    {
        this.instance = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (args.length > 0)
        {
            if (args[0].equalsIgnoreCase("net"))
            {
                return new Rm_SpawnNetCommand(instance).onCommand(sender, command, label, args);
            }
        }
        return true;
    }
}
