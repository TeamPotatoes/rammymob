package com.teampotatoes.rammycraft.rammymob.Listeners;

import com.teampotatoes.rammycraft.rammymob.RammyMob;
import com.teampotatoes.rammycraft.rammymob.TexturePack;
import de.tr7zw.nbtapi.NBTEntity;
import de.tr7zw.nbtapi.NBTItem;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Bee;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Panda;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class InteractListener implements Listener
{
    private final RammyMob instance;

    // List available entities for capture net
    private final EntityType[] availableEntities = {
            EntityType.PANDA,
            EntityType.BEE,
    };

    private ItemMeta addGlowEffect(ItemMeta itemMeta)
    {
        // Add glow effect
        itemMeta.addEnchant(Enchantment.LURE, 1, false);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return itemMeta;
    }

    private String formatLore(String key, String value)
    {
        // Set lore colors
        return ChatColor.GOLD + key + ": " + ChatColor.YELLOW + value;
    }

    public InteractListener(RammyMob instance)
    {
        this.instance = instance;
    }

    /**
     * Logic to save entity into capture net
     *
     * @param event
     */
    @EventHandler
    public void onCapture(PlayerInteractEntityEvent event)
    {
        if (event.getHand() == EquipmentSlot.OFF_HAND) return;
        Entity entity = event.getRightClicked();
        // Can only capture on ExploreWorld
        if (!event.getPlayer().getWorld().getName().equalsIgnoreCase("world_1")) return;
        PlayerInventory playerInventory = event.getPlayer().getInventory();
        ItemStack mainHandItem = playerInventory.getItemInMainHand();
        if (mainHandItem == null) return;
        if (mainHandItem.getType() != Material.LEAD) return;
        NBTItem nbtItem = new NBTItem(mainHandItem);
        int isCaptureNet = 0;
        try
        {
            isCaptureNet = nbtItem.getInteger("isCaptureNet");
        } finally
        {
            if (isCaptureNet == 0) return;
        }
        // Cancel event interact
        event.setCancelled(true);
        EntityType entityType = entity.getType();

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if (ArrayUtils.contains(availableEntities, entityType))
                {
                    NBTEntity nbtEntity = new NBTEntity(entity);
                    // Get player's held item
                    ItemMeta mainItemMeta = mainHandItem.getItemMeta();
                    // Add glow effect
                    mainItemMeta = addGlowEffect(mainItemMeta);

                    ArrayList<String> lores = new ArrayList<>();
                    lores.add(ChatColor.GOLD + "Right click on land to release it. (1 time use)");

                    NBTItem captureNet = null;

                    // Panda
                    if (entityType == EntityType.PANDA)
                    {
                        Panda panda = (Panda) entity;

                        // Get panda's attributes
                        String mainGene = nbtEntity.getString("MainGene");
                        String hiddenGene = nbtEntity.getString("HiddenGene");
                        Double health = panda.getHealth();
                        int age = panda.getAge();

                        // Add lores
                        lores.add(formatLore("Entity", "Panda"));
                        lores.add(formatLore("Main Gene", StringUtils.capitalize(mainGene)));
                        lores.add(formatLore("Hidden Gene", StringUtils.capitalize(hiddenGene)));
                        lores.add(formatLore("Health", health.toString()));

                        // Add data for texture pack
                        mainItemMeta.setCustomModelData(TexturePack.CAPTURENET_PANDA.getId());

                        // Save entity data to item on hand
                        mainItemMeta.setLore(lores);
                        mainHandItem.setItemMeta(mainItemMeta);

                        captureNet = new NBTItem(mainHandItem);
                        captureNet.setString("entity", EntityType.PANDA.toString());
                        captureNet.setString("mainGene", mainGene);
                        captureNet.setString("hiddenGene", hiddenGene);
                        captureNet.setDouble("health", health);
                        captureNet.setInteger("age", age);
                        // Disable capture net
                        captureNet.setInteger("isCaptureNet", 0);
                    }
                    // Bee
                    else if (entityType == EntityType.BEE)
                    {
                        Bee bee = (Bee) entity;

                        int anger = nbtEntity.getInteger("Anger");
                        int cannotEnterHiveTicks = nbtEntity.getInteger("CannotEnterHiveTicks");
                        boolean hasNectar = nbtEntity.getBoolean("HasNectar");
                        boolean hasStung = nbtEntity.getBoolean("HasStung");
                        Double health = bee.getHealth();
                        int age = bee.getAge();

                        // Add lores
                        lores.add(formatLore("Entity", "Bee"));
                        lores.add(formatLore("Health", health.toString()));

                        // Add data for texture pack
                        mainItemMeta.setCustomModelData(TexturePack.CAPTURENET_BEE.getId());

                        // Save entity data to item on hand
                        mainItemMeta.setLore(lores);
                        mainHandItem.setItemMeta(mainItemMeta);

                        captureNet = new NBTItem(mainHandItem);

                        captureNet.setString("entity", EntityType.BEE.toString());
                        captureNet.setInteger("anger", anger);
                        captureNet.setInteger("cannotEnterHiveTicks", cannotEnterHiveTicks);
                        captureNet.setBoolean("hasNectar", hasNectar);
                        captureNet.setBoolean("hasStung", hasStung);
                        captureNet.setDouble("health", health);
                        captureNet.setInteger("age", age);
                        // Disable capture net
                        captureNet.setInteger("isCaptureNet", 0);
                    }

                    // Update item on hand
                    if (captureNet != null)
                    {
                        playerInventory.setItemInMainHand(new ItemStack(Material.AIR));
                        playerInventory.setItemInMainHand(captureNet.getItem());
                        entity.remove();
                    }
                }
            }
        }.runTaskLater(instance, 1);
    }

    /**
     * Logic to respawn mob when user right click the capture net on land
     *
     * @param event
     */
    @EventHandler
    public void onRelease(PlayerInteractEvent event)
    {
        if (event.getAction() == Action.RIGHT_CLICK_AIR) return;
        if (event.getHand() == EquipmentSlot.OFF_HAND) return;
        ItemStack mainHandItem = event.getItem();
        if (mainHandItem == null) return;
        if (mainHandItem.getType() != Material.LEAD) return;
        NBTItem nbtItem = new NBTItem(mainHandItem);
        String entityType = nbtItem.getString("entity");
        if (entityType.isEmpty()) return;
        EntityType entity = EntityType.valueOf(nbtItem.getString("entity"));

        // Check if entity is in the list
        if (ArrayUtils.contains(availableEntities, entity))
        {
            try
            {
                // Cancel interact event
                event.setCancelled(true);
                // Common entity attributes
                Double health = nbtItem.getDouble("health");
                int age = nbtItem.getInteger("age");
                // Get clicked location and add 1 to y to prevent spawning in the block
                Location clickedLocation = event.getClickedBlock().getLocation();
                clickedLocation.add(0, 1, 0);

                switch (entity)
                {
                    case PANDA:
                        String mainGene = nbtItem.getString("mainGene");
                        String hiddenGene = nbtItem.getString("hiddenGene");
                        try
                        {
                            Panda panda = (Panda) event.getPlayer().getWorld().spawnEntity(clickedLocation, EntityType.PANDA);
                            panda.setMainGene(Panda.Gene.valueOf(mainGene.toUpperCase()));
                            panda.setHiddenGene(Panda.Gene.valueOf(hiddenGene.toUpperCase()));
                            panda.setAge(age);
                            panda.setHealth(health);
                        } catch (Exception e)
                        {

                        }
                        break;
                    case BEE:
                        int anger = nbtItem.getInteger("anger");
                        int cannotEnterHiveTicks = nbtItem.getInteger("cannotEnterHiveTicks");
                        boolean hasNectar = nbtItem.getBoolean("hasNectar");
                        boolean hasStung = nbtItem.getBoolean("hasStung");
                        try
                        {
                            Bee bee = (Bee) event.getPlayer().getWorld().spawnEntity(clickedLocation, EntityType.BEE);
                            bee.setAnger(anger);
                            bee.setCannotEnterHiveTicks(cannotEnterHiveTicks);
                            bee.setHasNectar(hasNectar);
                            bee.setHasStung(hasStung);
                            bee.setAge(age);
                            bee.setHealth(health);
                        } catch (Exception e)
                        {

                        }
                        break;
                }

                // Remove capture net
                event.getPlayer().getInventory().setItemInMainHand(new ItemStack((Material.AIR)));

            } catch (Exception e)
            {

            }
        }

    }
}
