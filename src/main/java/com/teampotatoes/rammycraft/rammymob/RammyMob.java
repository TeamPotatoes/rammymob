package com.teampotatoes.rammycraft.rammymob;

import com.teampotatoes.rammycraft.rammymob.Commands.RmCommand;
import com.teampotatoes.rammycraft.rammymob.Listeners.InteractListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class RammyMob extends JavaPlugin
{

    @Override
    public void onEnable()
    {
        // Plugin startup logic
        Bukkit.getServer().getPluginManager().registerEvents(new InteractListener(this), this);
        this.getCommand("rm").setExecutor(new RmCommand(this));
    }

    @Override
    public void onDisable()
    {
        // Plugin shutdown logic
    }
}
